### Nike Interview Frontend Code Challenge [Vue]

_The provided code document should contain more details._

## Project setup
```
npm install
```

### Compiles and hot-reloads for development (Port 8080)
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

You need to start backend server (on port 8081), to be able to see live data in the frontend app.

### Change & Code improvements 

1. I removed the const `shoe_database` since that it is hardcoded in the `NikeShoes.vue`. Most of the database management actions should be handled by the backend server instead of letting frontend to do such work.
2. I also noticed that if the shoe price go into negative, it would be no proper action to handle such exceptions. I have added a additional `getShoeData` functional to filter out negative values to prevent unnoriml cases appearance on webpage
3. I also changed the fetch function from requesting data by shoe's id each time to requesting a list of shoedatas at a time.

### If I had more time, what would you add futher

1. I would probably make the website look more fancy, since right now it is just a plain website.

   

### What were your doubts, and what were your assumptions for the projects?

1. at the first glance, I thought I was going to make a website like this as a whole with no code gived.
2. I thought that I would hardly finish this project since it was only given for one day to complete.

### Other notes

1. I took roughly 2 hour to get it running on my computer, most of the reasons is because the node version of this project is much higher that the one I am using. Took me a while to figure out the reason. 

